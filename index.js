//variable declaration
var username = 'Asrul Kadir'; // var
let age = 23; // let
const isMarried = false; // constanta

// variable array
const names = ['Asrul', 'Kadir', 'Rizki']; // array string
const numbers = [1, 2, 3, 4, 5]; // array number
const mixed = ['Asrul', 1, true]; // array mixed

// variable object
const person = {
  firstName: 'Asrul',
  lastName: 'Kadir',
  age: 23,
  isMarried: false,
};

// variable object
const person2 = {
  firstName: 'Asrul',
  lastName: 'Kadir',
  age: 23,
  isMarried: false,
  hobbies: ['movies/animes', 'football', 'coding'],
  address: {
    city: 'Pinrang',
    state: 'South Sulawesi',
  },
};

// variable array object
const person3 = [
  {
    firstName: 'Asrul',
    lastName: 'Kadir',
    age: 23,
    isMarried: false,
    hobbies: ['movies/animes', 'football', 'coding'],
    address: {
      city: 'Pinrang',
      state: 'South Sulawesi',
    },
  },
  {
    firstName: 'Andik',
    lastName: 'Kurniawan',
    age: 26,
    isMarried: true,
    hobbies: ['photografy', 'reading', 'traveling'],
    address: {
      city: 'Pinrang',
      state: 'South Sulawesi',
    },
  },
];

// alrogithm insertion sort
function insertionSort(inputArr) {
  let n = inputArr.length;
  for (let i = 1; i < n; i++) {
    let current = inputArr[i];
    let j = i - 1;
    while (j > -1 && current < inputArr[j]) {
      inputArr[j + 1] = inputArr[j];
      j--;
    }
    inputArr[j + 1] = current;
  }
  return inputArr;
}

let inputArr = [65, 5, 9, 4, 23, 6, 74, 23, 9, 11];
const sorting = inputArr.sort((a, b) => a - b);
console.log(insertionSort(inputArr));
console.log(sorting);

// algorithm binary search
function binarySearch(inputArr, searchValue) {
  let low = 0;
  let high = inputArr.length - 1;
  let mid;
  while (low <= high) {
    mid = Math.floor((low + high) / 2);
    if (inputArr[mid] === searchValue) {
      return mid;
    } else if (inputArr[mid] < searchValue) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }
  return -1;
}

const search = binarySearch(sorting, 6);
console.log(search);

// branching
const branch = 'Asrul';
if (branch === 'Asrul') {
  console.log('Hello Asrul');
} else if (branch === 'Andik') {
  console.log('Hello Andik');
} else {
  console.log('Hello Guest');
}

// switch statement
const branch2 = 'Asrul';
switch (branch2) {
  case 'Asrul':
    console.log('Hello Asrul');
    break;
  case 'Andik':
    console.log('Hello Andik');
    break;
  default:
    console.log('Hello Guest');
    break;
}

// for loop
for (let i = 0; i < 10; i++) {
  console.log(i);
}

// while loop
let i = 0;
while (i < 10) {
  console.log(i);
  i++;
}

// function declaration
function greet(name, age) {
  console.log(`Hello ${name} you are ${age} years old`);
}
greet('Asrul', 23);

// function expression
const operation = (a, b) => a + b;
console.log(operation(2, 3));

// print variable
console.log(person);
console.log(person2.address.city);

// print array
console.log(names);

// print object
console.log(person2.hobbies[1]);

// print object
console.log(person2.address.state);

// print string
console.log(username);

// print number
console.log(age);
